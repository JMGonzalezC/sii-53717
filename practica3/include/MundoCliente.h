// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoCliente  
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
//Datos de Memoria compartida
	char DMC[15] = "/tmp/DMC";
	DatosMemCompartida dmc;
	DatosMemCompartida * pdmc;
	int cont_au= 0; 		//temoporizador ausencia
//Datos de objetos en CMUNDO
	ListaEsferas esferas;
	std::vector<Plano> paredes;
	Esfera esfera;
	Esfera esfera2;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int puntos1;
	int puntos2;
//Fifo y strings servidor-cliente
	char cord[400];					//Array de datos a transmitir
	char inf_esferas[200];				//Array auxiliar para datos de "ListaEsferas"
//Fifo cliente-servidor
	char comand[20];
//Socket
	Socket sock_com;
	char ip[15] = "127.0.0.1";
	int port = 3900;
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
