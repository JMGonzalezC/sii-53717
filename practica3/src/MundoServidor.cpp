// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void * hilo_comandos(void *d);

CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{

	sock_com.Close();
	sock_conec.Close();
	
}

void CMundoServidor::InitGL()
{

	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{

	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{

	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esferas.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{
	
	char *buff;
	conta ++;
	
//Esferas nuevas
	if(conta > 400)
	{
	conta = 0;
	Esfera * aux = new Esfera();
	esferas.Agregar(aux);
	}
//MOvimiento de objetos
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esferas.Mueve(0.025f);
//Interaccion entre objetos
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esferas);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esferas);
	jugador2.Rebota(esferas);
	esferas.Rebote();

	for(int j=0; j < esferas.getNumero(); j++)
	{
		if(fondo_izq.Rebota((*esferas[j])))
		{
			if(esferas.getNumero()>1){
			esferas.Eliminar(esferas[j]);
			}
			else{
			esferas[j]->centro.x=0;
			esferas[j]->centro.y=rand()/(float)RAND_MAX;
			esferas[j]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[j]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			}
			puntos2++;
			asprintf(&buff, "Jugador2 marca 1 punto. TOTAL: %i puntos\n", puntos2);
			write(fd, buff, strlen(buff)+1);	
		}

		if(fondo_dcho.Rebota((*esferas[j])))
		{
			if(esferas.getNumero()>1){
			esferas.Eliminar(esferas[j]);
			}else
			{
			esferas[j]->centro.x=0;
			esferas[j]->centro.y=rand()/(float)RAND_MAX;
			esferas[j]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esferas[j]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
			}
			puntos1++;
			asprintf(&buff, "Jugador1 marca 1 punto. TOTAL: %i puntos\n", puntos1);
			write(fd, buff, strlen(buff)+1);
			free(buff);
		}		
	}
//Actualización y escritura de coordenadas
	asprintf(&buff, "%d ", esferas.numero);
	strncat(inf_esferas,buff,strlen(buff)+1);
	for(int i=0; i<esferas.numero;i++){
	asprintf(&buff, "%f %f ", esferas[i]->centro.x, esferas[i]->centro.y);
	strncat(inf_esferas,buff,strlen(buff)+1);
	}
	sprintf(cord,"%f %f %f %f %f %f %f %f %d %d %s", jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2, puntos1, puntos2, inf_esferas );
	memset(inf_esferas,0,sizeof(inf_esferas));
	sock_com.Send(cord,sizeof(cord));


//fin de juego
	if(puntos1 > 5){
	printf("Gana el jugador 1");
	exit(0);
	}
	if(puntos2 > 5){
	printf("Gana el juegador 2");
	exit(0);
	}

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{

//	switch(key)
//	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;
//
//	}
}

void CMundoServidor::Init()
{

	char cad[20]="no actualiza";

//iniciar el fifo logger
	fd= open(fifo, O_WRONLY);
	if(fd==-1){
	perror("error Mundo: apertura fifo logger");
	}

//iniciar el socket de coneccion
	sock_conec.InitServer(ip, port);
	sock_com = sock_conec.Accept();
	sock_com.Receive(cad,sizeof(cad));
	printf("Conectado a: %s \n", cad);

//inicializar el thread
	pthread_t thid1;
	pthread_create(&thid1,NULL,hilo_comandos,this);

//declarar plano
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//esfera
	Esfera *e1 = new Esfera();
	esferas.Agregar(e1);
}

void CMundoServidor::RecibeComandosJugador()
{

	while(1){
	usleep(10);
	char cad[15];
	sock_com.Receive(cad,sizeof(cad));
	unsigned char key;
	sscanf(cad, "%c", &key);
		switch(key){
			case 's':
			jugador1.velocidad.y=-4;
			break;
			case 'w':
			jugador1.velocidad.y=4;
			break;
			case 'o':
			jugador2.velocidad.y=4;
			break;
			case 'l':
			jugador2.velocidad.y=-4;
			break;
			default: break;
		}	
	}
}

void * hilo_comandos(void *d){

	CMundoServidor * p= (CMundoServidor*) d;
	p->RecibeComandosJugador();
}
