#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
char fifo[30]= "/tmp/fifo_ten";
int fd;
char str[20];
	if(mkfifo(fifo,0666)==0)
	{
	 fd= open(fifo, O_RDONLY);
	 if(fd==-1){
		perror("error: open fifo");
		return 1;
		}
	while(read(fd,str,sizeof(str))>0){
		write(1,str,strlen(str)+1);
		}
	if(close(fd)==-1){ perror("error: cerrar fifo"); return 1;}
	if(unlink(fifo)==-1){ perror("error: eliminar fifo"); return 1;}
	return 0;
	}
	else
	{
	 perror("error: makefifo()");
	}


}
