// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoServidor  
{

public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();
//Datos de objetos en CMUNDO
	ListaEsferas esferas;
	std::vector<Plano> paredes;
	Esfera esfera;
	Esfera esfera2;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int puntos1;
	int puntos2;
	int conta = 0;			//temporizador para esferas nuevas
//Fifo y strings logger
	char fifo[20] = "/tmp/fifo_ten";	//identificador del fifo logger
	int fd;					//file descriptor del fifo logger
//Fifo y strings servidor-cliente
	char cord[400];
	char inf_esferas[200] ={0};
//Sockets servidor
	Socket sock_com;
	Socket sock_conec;
	char ip[15] = "127.0.0.1";
	int port = 3900;	

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
