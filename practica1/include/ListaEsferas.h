#pragma once
#include "Esfera.h"
#define MAX_ESFERAS 100



class ListaEsferas
{
public:
	ListaEsferas();
	~ListaEsferas();
	bool Agregar(Esfera * e);
	void Dibuja();
	void Mueve(float t);

	
	void Rebote();
	void DestruirContenido();
	void Eliminar(int index);
	void Eliminar(Esfera *);

	Esfera * operator [](int i);

	int getNumero() { return numero; }
private:
	Esfera * lista[MAX_ESFERAS];
	int numero;
	
};


