// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
	
}

CMundoCliente::~CMundoCliente()
{
	
	munmap(pdmc,sizeof(DatosMemCompartida));
	if(unlink("/tmp/DMC")==-1){ perror("error: eliminar fifo"); return ;}
	sock_com.Close();
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esferas.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	int num_esf=1;
	cont_au++;
	char buff_borrar[30];
//Lectura y actualizacion de los datos del servidor
	sock_com.Receive(cord,sizeof(cord));	
	sscanf(cord,"%f %f %f %f %f %f %f %f %d %d %[^\n]", &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2, inf_esferas );

//Lectura del número de esferas actualizado	
	sscanf(inf_esferas,"%d",&num_esf);
	sprintf(buff_borrar,"%d",num_esf);
	memmove (inf_esferas, inf_esferas + strlen(buff_borrar) + 1, strlen(inf_esferas));	

//Agregar esfera en el cliente
	while(esferas.numero < num_esf){
	Esfera *e1 = new Esfera();
	esferas.Agregar(e1);
	}
//Eliminar esfera en el cliente
	while(esferas.numero > num_esf){
	num_esf= esferas.numero;
	esferas.Eliminar(esferas[esferas.numero -1]);
	}

//Separación y lectura del array de la lista de esferas
	for(int i=0; i<esferas.numero;i++){
	sscanf(inf_esferas,"%f %f", &esferas[i]->centro.x, &esferas[i]->centro.y);
	sprintf(buff_borrar,"%f %f",esferas[i]->centro.x, esferas[i]->centro.y);
	memmove (inf_esferas, inf_esferas + strlen(buff_borrar) +1, strlen(inf_esferas)-strlen(buff_borrar));

	}

//Deteccion esfera más cercana RAQUETA1


if (esferas.getNumero() <= 1) esfera = (*esferas[0]);
else	for(int i=0; i < esferas.getNumero()-1; i++){
		if(esferas[i]->centro.x < esferas[i+1]->centro.x){ 
		esfera = (*esferas[i]);
		}
		else {
		esfera = (*esferas[i+1]);
		}
	}
if(cont_au > 200){
	pdmc->ausente=1;
	if (esferas.getNumero() <= 1) esfera2 = (*esferas[0]);
	else	for(int i=0; i < esferas.getNumero()-1; i++){
			if(esferas[i]->centro.x > esferas[i+1]->centro.x){ 
			esfera2 = (*esferas[i]);
			}
			else {
			esfera2 = (*esferas[i+1]);
			}

	}
dmc.esfera2 = esfera2;
dmc.raqueta2 = jugador2;
(*pdmc).esfera2=dmc.esfera2;
(*pdmc).raqueta2=dmc.raqueta2;
}

//Actualizacion de DMC
dmc.esfera = esfera;
dmc.raqueta1 = jugador1;
(*pdmc).esfera=dmc.esfera;
(*pdmc).raqueta1=dmc.raqueta1;

dmc.esfera2 = esfera2;
dmc.raqueta2 = jugador2;
(*pdmc).esfera2=dmc.esfera2;
(*pdmc).raqueta2=dmc.raqueta2;
		
//Actuacion de la raqueta
	switch (pdmc->accion)
	{
	case 1: OnKeyboardDown('w',0,0); break;
	case -1: OnKeyboardDown('s',0,0); break;
	default: break;
	}
//Accion  ausencia jugador 2
if(pdmc->ausente!=0){

	switch (pdmc->accion2)
	{
	case 1: OnKeyboardDown('o',0,0); break;
	case -1: OnKeyboardDown('l',0,0); break;
	default: break;
	}
	pdmc->ausente = 1;
	cont_au=1000;
}

//fin de juego
	if(puntos1 > 5){
	printf("Gana el jugador 1");
	exit(0);
	}
	if(puntos2 > 5){
	printf("Gana el jugador 2");
	exit(0);
	}



}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{

	case 's':
		sprintf(comand,"%c",key);
		break;
	case 'w':
		sprintf(comand,"%c",key);
		break;
	case 'l':
		sprintf(comand,"%c",key);
		cont_au=0;pdmc->ausente=0; break;
	case 'o':
		sprintf(comand,"%c",key);
		cont_au=0;pdmc->ausente=0; break;
	default: break;
	}
	
	sock_com.Send(comand,sizeof(comand));
}

void CMundoCliente::Init()
{
	char client[20];
//inicializacion del socket de comunicacion cliente-servidir
	printf("Introduzca nombre del cliente: ");
	scanf("%s", client);
	sock_com.Connect(ip,port);
	sock_com.Send(client,sizeof(client));

//declarar plano
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//esfera
	Esfera *e1 = new Esfera();
	esferas.Agregar(e1);

//creacion del fichero para proyeccion
int fdmem;
	if((fdmem=open(DMC,O_CREAT|O_TRUNC|O_RDWR,0666))<0)
	{
	perror("Error: apertura DMC");
	return;
	}
//truncar fichero a tanaño del DMC
	if(ftruncate(fdmem,sizeof(dmc))<0)
	{
	perror("Error: trancamiento");
	close(fdmem); unlink(DMC);
	return;
	}
//Proyectar en memoria
	if((pdmc= static_cast<DatosMemCompartida*>(mmap((caddr_t)0,sizeof(dmc),PROT_WRITE|PROT_READ,MAP_SHARED,fdmem,0))) == MAP_FAILED)
	{
	perror("Error: proyeccion en memoria");
	close(fdmem); unlink(DMC);
	return;
	}
close (fdmem);

}
