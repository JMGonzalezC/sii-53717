#include "ListaEsferas.h"



ListaEsferas::ListaEsferas()
{
	numero = 0;
	for (int i = 0; i < MAX_ESFERAS; i++) {
		lista[i] = 0;
	}
}


ListaEsferas::~ListaEsferas()
{
}

bool ListaEsferas::Agregar(Esfera * e) {
	if (numero < MAX_ESFERAS) 
	{
		for (int i = 0; i < numero; i++)
		{
			if (lista[i] == e)  return false; 
		
		}
		lista[numero++] = e;
	}
	else {
		return false;
	}
	return true;
}

void ListaEsferas::Mueve(float t) {
	for (int i = 0; i < numero; i++) {

		lista[i]->Mueve(t);
	}

}

void ListaEsferas::Dibuja() {
	for (int i = 0; i < numero; i++) {

		lista[i]->Dibuja();
	}

}
 	




void ListaEsferas::Rebote(){

	for (int i = 0; i < numero; i++) 
	{
		for (int j = 0; j < numero; j++) 
		{
			if (i != j) 
			{
				lista[i]->Rebote(*(lista[j]));				
				//Interaccion::rebote(*(lista[i]), *(lista[j]));
			}	
		}
	}
}

void ListaEsferas::DestruirContenido() 
{
	for (int i = 0; i < numero; i++)
	{
		delete lista[i];
	}

	numero = 0;
}

void ListaEsferas::Eliminar(int index)
{
	if ((index<0) || (index >= numero))   return;  
	delete lista[index]; 
	numero--; 
	for (int i = index; i < numero; i++)
	{
		lista[i] = lista[i + 1];
	}


}

void ListaEsferas::Eliminar(Esfera *e) 
{ 
	for (int i = 0; i < numero; i++)
	{
		if (lista[i] == e) { Eliminar(i);      return; }
	}

}

Esfera * ListaEsferas::operator[](int i)
{
	if (i >= numero)//si me paso, devuelvo la ultima  
		i=numero-1; 

	if (i<0) //si el indice es negativo, devuelvo la primera  
			i=0;   
	return lista[i];
}

